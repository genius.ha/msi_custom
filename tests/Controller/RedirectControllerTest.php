<?php

namespace Drupal\msi_custom\Tests;

use Drupal\simpletest\WebTestBase;

/**
 * Provides automated tests for the msi_custom module.
 */
class RedirectControllerTest extends WebTestBase {


  /**
   * {@inheritdoc}
   */
  public static function getInfo() {
    return [
      'name' => "msi_custom RedirectController's controller functionality",
      'description' => 'Test Unit for module msi_custom and controller RedirectController.',
      'group' => 'Other',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();
  }

  /**
   * Tests msi_custom functionality.
   */
  public function testRedirectController() {
    // Check that the basic functions of module msi_custom.
    $this->assertEquals(TRUE, TRUE, 'Test Unit Generated via Drupal Console.');
  }

}
