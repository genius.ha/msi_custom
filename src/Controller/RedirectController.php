<?php

namespace Drupal\msi_custom\Controller;

use Drupal\Core\Controller\ControllerBase;

use Symfony\Component\HttpFoundation\RedirectResponse;
use \Drupal\Core\Url;
/**
 * Class RedirectController.
 */
class RedirectController extends ControllerBase {

  /**
   * Redirect.
   *
   * @return string
   *   Return Hello string.
   */
  public function Siteredirect() {
    $path = Url::fromRoute('entity.node.canonical', ['node' => 16], ['absolute' => TRUE])->toString();
    return new RedirectResponse($path);
  }

}
